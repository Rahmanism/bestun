# APIs available in this project

### /submit/expense/

POST, returns a json  
input: date (optional), text, amount, token  
output: status: ok  


### /submit/income/

POST, returns a json  
input: date (optional), text, amount, token  
output: status: ok  


### /accounts/register/

**step 1:**  
POST  
input: username, email, password  
output: sends an confirmation emai  l
  
**step 2:** # click on confirmation link in the email  
input: email, token  
output: shows the token  

### /accounts/login/

  POST, returns a json  
  input: username, password  
  output: status:ok & token  


### /q/generalstats/

POST, returns json  
input: token, fromdate(optional), todate(optional) # dates not implemented yet  
output: some stats in json for the user with the given token  
description: it'll give you a simple summary of your financial transactions.  


### /q/expenses/

POST, returns json  
input: token, count(optional, default is 10)  
output: last count ones expenses in json for the user with the given token  
description: It gives last count expenses for the user.  


### /q/incomes/

POST, returns json  
input: token, count(optional, default is 10)  
output: last count ones incomes in json for the user with the given token  
description: It gives last count incomes for the user.  


### /q/notes/

GET, returns json  
input: count(optional, default is 10)  
output: notes in json  
description: it'll give you a list of last notes.  


