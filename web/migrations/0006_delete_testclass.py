# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-01-13 07:48
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0005_passwordresetcodes'),
    ]

    operations = [
        migrations.DeleteModel(
            name='TestClass',
        ),
    ]
