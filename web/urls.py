""" Betun web project url routes """

from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^submit/expense/?$', views.submit_expense),
    url(r'^submit/income/?$', views.submit_income),
    url(r'^accounts/register/?$', views.register),
    url(r'^accounts/login/?$', views.login),
    url(r'^q/generalstats/?$', views.generalstats),
    url(r'^q/expenses/?$', views.expenses),
    url(r'^q/incomes/?$', views.incomes),
    url(r'^q/expense/?$', views.expense),
    url(r'^q/income/?$', views.income),
    url(r'^q/notes/(?P<count>[0-9]+)?$', views.notes),
    url(r'^$', views.index),
]
