
import random
import string
from datetime import datetime
import requests

from django.conf import settings
from django.contrib.auth.hashers import check_password, make_password
from django.core import serializers
from django.core.mail import EmailMessage
from django.db.models import Count, Sum
from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from .models import Expense, Income, Passwordresetcodes, Token, User, Note
from .utils import RateLimited
from .utils import get_client_ip

# Create your views here.

RANDOM_STR = lambda N: ''.join(random.SystemRandom().choice(
    string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(N))

# Default number of queries count
QueryCount = 10


def index(request):
    """Returns the index page of web site."""
    context = {}
    return render(request, 'index.html', context)


# TODO: validate token everywhere it's used.

@csrf_exempt
@require_POST
def generalstats(request):
    """Returns some income and expense stats in json format"""
    # TODO: validate the date range
    this_token = request.POST['token']
    this_user = User.objects.filter(token__token=this_token).get()
    # pylint: disable=no-member
    incomes = Income.objects.filter(user=this_user).aggregate(
        Count('amount'), Sum('amount'))
    # pylint: disable=no-member
    expenses = Expense.objects.filter(user=this_user).aggregate(
        Count('amount'), Sum('amount'))
    context = {}
    context['incomes'] = incomes
    context['expenses'] = expenses
    return JsonResponse(context)


@csrf_exempt
@require_POST
def expenses(request):
    """Returns the list of expenses."""

    this_token = request.POST['token']
    query_count = int(request.POST.get('count', QueryCount))
    this_user = get_object_or_404(User, token__token=this_token)
    expenses_list = Expense.objects.filter(
        user=this_user).order_by('-date')[:query_count]
    expenses_serialized = serializers.serialize('json', expenses_list)
    return JsonResponse(expenses_serialized, safe=False)


@csrf_exempt
@require_POST
def incomes(request):
    """Returns the list of incomes."""

    this_token = request.POST['token']
    query_count = int(request.POST.get('count', QueryCount))
    this_user = get_object_or_404(User, token__token=this_token)
    incomes_list = Income.objects.filter(
        user=this_user).order_by('-date')[:query_count]
    incomes_serialized = serializers.serialize('json', incomes_list)
    return JsonResponse(incomes_serialized, safe=False)


@csrf_exempt
@require_POST
def expense(request):
    """Returns an expense with the given id."""

    this_token = request.POST['token']
    this_id = request.POST['id']
    this_user = get_object_or_404(User, token__token=this_token)
    expense = Expense.objects.filter(user=this_user, id=this_id)
    expense_serialized = serializers.serialize('json', expense)
    return JsonResponse(expense_serialized, safe=False)


@csrf_exempt
@require_POST
def income(request):
    """Returns an income with the given id."""

    this_token = request.POST['token']
    this_id = request.POST['id']
    this_user = get_object_or_404(User, token__token=this_token)
    income = Income.objects.filter(user=this_user, id=this_id)
    income_serialized = serializers.serialize('json', income)
    return JsonResponse(income_serialized, safe=False)


@csrf_exempt
def notes(request, count):
    """Returns the list of notes."""

    # if nothing passed as count set the default count
    if count:
        c = int(count)
    else:
        c = QueryCount

    notes_list = Note.objects.all().order_by('-date')[:c]
    notes_serialized = serializers.serialize('json', notes_list)
    return JsonResponse(notes_serialized, safe=False)


@csrf_exempt
@require_POST
def submit_expense(request):
    """user submits an expense"""

    this_token = request.POST['token']
    this_user = User.objects.filter(token__token=this_token).get()

    if 'date' in request.POST:
        now = request.POST['date']
    else:
        now = datetime.now()
    # pylint: disable=no-member
    Expense.objects.create(user=this_user,
                           amount=request.POST['amount'],
                           text=request.POST['text'],
                           date=now)

    return JsonResponse({
        'status': 'ok'
    })


@csrf_exempt
@require_POST
def submit_income(request):
    """user submits an income"""

    this_token = request.POST['token']
    this_user = User.objects.filter(token__token=this_token).get()

    if 'date' in request.POST:
        now = request.POST['date']
    else:
        now = datetime.now()
    # pylint: disable=no-member
    Income.objects.create(user=this_user,
                          amount=request.POST['amount'],
                          text=request.POST['text'],
                          date=now)

    return JsonResponse({
        'status': 'ok'
    })


@require_POST
def grecaptcha_verify(request):
    """Verifies the Google CAPTCHA"""
    data = request.POST
    captcha_rs = data.get('g-recaptcha-response')
    url = "https://www.google.com/recaptcha/api/siteverify"
    params = {
        'secret': settings.RECAPTCHA_SECRET_KEY,
        'response': captcha_rs,
        'remoteip': get_client_ip(request)
    }
    verify_rs = requests.get(url, params=params, verify=True)
    verify_rs = verify_rs.json()
    return verify_rs.get("success", False)


def register(request):
    """A new user will be registered using this method."""
    list(request.POST)
    # request.POST.has_key('requestcode')
    if 'requestcode' in request.POST:
        # form is filled. if not spam, generate code and save in db,
        # wait for email confirmation, return message
        # is this spam? check reCaptcha
        if not grecaptcha_verify(request):  # captcha was not correct
            # TODO: forgot password
            context = {
                'message':
                'کپچای گوگل درست وارد نشده بود. شاید ربات هستید؟' +
                ' کد یا کلیک یا تشخیص عکس زیر فرم' +
                ' را درست پر کنید. ببخشید که فرم به شکل اولیه برنگشته!'}
            return render(request, 'register.html', context)

        # duplicate email
        if User.objects.filter(email=request.POST['email']).exists():
            # TODO: forgot password
            context = {'message': 'متاسفانه این ایمیل قبلا استفاده شده است. در صورتی که این ایمیل شما است، از صفحه ورود گزینه فراموشی پسورد رو انتخاب کنین. ببخشید که فرم ذخیره نشده. درست می شه'}
            # TODO: keep the form data
            return render(request, 'register.html', context)

        # if user does not exists
        if not User.objects.filter(username=request.POST['username']).exists():
            code = RANDOM_STR(28)
            now = datetime.now()
            email = request.POST['email']
            password = make_password(request.POST['password'])
            username = request.POST['username']
            temporarycode = Passwordresetcodes(
                email=email, time=now, code=code, username=username, password=password)
            temporarycode.save()
            message = EmailMessage("فعال سازی اکانت بستون",
                                   "برای فعال سازی ایمیلی بستون خود روی لینک روبرو کلیک کنید: " +
                                   "{}?email={}&code={}"
                                   .format(request.build_absolute_uri('/accounts/register/'),
                                           email, code),
                                   "rahmani@rahmanism.ir",
                                   [email])
            message.send()
            context = {
                'message': 'ایمیلی حاوی لینک فعال سازی اکانت به شما' +
                ' فرستاده شده، لطفا پس از چک کردن ایمیل، روی لینک کلیک کنید.'}
            return render(request, 'index.html', context)
        else:
            # TODO: forgot password
            context = {
                'message': 'متاسفانه این نام کاربری قبلا استفاده شده است. از نام کاربری دیگری استفاده کنید. ببخشید که فرم ذخیره نشده. درست می شه'}
            # TODO: keep the form data
            return render(request, 'register.html', context)
    # user clicked on code # request.GET.has_key('code')
    elif 'code' in request.GET:
        email = request.GET['email']
        code = request.GET['code']
        # if code is in temporary db, read the data and create the user
        if Passwordresetcodes.objects.filter(code=code).exists():
            new_temp_user = Passwordresetcodes.objects.get(code=code)
            this_token = RANDOM_STR(48)
            newuser = User.objects.create(username=new_temp_user.username,
                                          password=new_temp_user.password, email=email)
            token = Token.objects.create(user=newuser, token=this_token)
            # maybe this will be essential:
            # newuser.token__token = token
            # delete the temporary activation code from db
            Passwordresetcodes.objects.filter(code=code).delete()
            context = {
                'message': 'اکانت شما فعال شد. توکن شما {} است. آن را ذخیره کنید. این توکن دیگر نمایش داده نخواهد شد.'.format(this_token)}
            return render(request, 'index.html', context)
        else:
            context = {
                'message': 'این کد فعال سازی معتبر نیست. در صورت نیاز دوباره تلاش کنید'}
            return render(request, 'register.html', context)
    else:
        context = {'message': ''}
        return render(request, 'register.html', context)


@csrf_exempt
@require_POST
#  @RateLimited(2), it doesn't check rate for just one user, so it should be modified in utils.py.
def login(request):
    if 'username' in request.POST and 'password' in request.POST:
        username = request.POST['username']
        password = request.POST['password']
        this_user = User.objects.get(username=username)
        if (check_password(password, this_user.password)):
            # return json with Token
            this_token = this_user.token  # Token.objects.get(user = this_user)
            context = {}
            context['result'] = 'ok'
            context['token'] = this_token.token
            return JsonResponse(context)
        else:
            # return error message
            context = {}
            context['result'] = 'error'
            return JsonResponse(context)


@csrf_exempt
@require_POST
def whoami(request):
    """Checks if there is a user with the given token."""

    this_token = request.POST['token']  # TODO: check if there is no 'token'
    # Check if there is a user with this token; or will return 404 instead.
    this_user = get_object_or_404(User, token__token=this_token)

    return JsonResponse({
        'user': this_user.username
    })
