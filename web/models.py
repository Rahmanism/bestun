""" Models for Bestun project """

from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Token(models.Model):
    """ token for every user for authentications """

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    token = models.CharField(max_length=48)

    def __str__(self):
        return "{} -- {}".format(self.user, self.token)


class Expense(models.Model):
    """ Model class for expenses """

    text = models.CharField(max_length=255)
    date = models.DateTimeField()
    amount = models.DecimalField(max_digits=12, decimal_places=2)
    user = models.ForeignKey(User)

    def __str__(self):
        return "{}, {} => {}".format(self.user, self.text, self.amount)


class Income(models.Model):
    """ Model class for incomes """

    text = models.CharField(max_length=255)
    date = models.DateTimeField()
    amount = models.DecimalField(max_digits=12, decimal_places=2)
    user = models.ForeignKey(User)

    def __str__(self):
        return "{}, {} => {}".format(self.user, self.text, self.amount)


class Passwordresetcodes(models.Model):
    """ Model for password reset when user forgets his/her password """

    code = models.CharField(max_length=32)
    email = models.CharField(max_length=120)
    time = models.DateTimeField()
    username = models.CharField(max_length=50)
    password = models.CharField(max_length=50)  # TODO: do not save password


class Note(models.Model):
    """ Model class for notes. Notes also will contains release notes. """

    title = models.CharField(max_length=255)
    body = models.CharField(max_length=4096)
    date = models.DateTimeField()

    def __str__(self):
        return self.title
