# -*- coding: utf-8 -*-

import time


def RateLimited(maxPerSecond):
    """A decorator. @RateLimited(10) will let 10 runs in 1 seconds"""

    minInterval = 1.0 / float(maxPerSecond)

    def decorate(func):
        lastTimeCalled = [0.0]

        def rateLimitedFunction(*args, **kargs):
            elapsed = time.clock() - lastTimeCalled[0]
            leftToWait = minInterval - elapsed
            if leftToWait > 0:
                time.sleep(leftToWait)
            ret = func(*args, **kargs)
            lastTimeCalled[0] = time.clock()
            return ret
        return rateLimitedFunction
    return decorate


def get_client_ip(request):
    """Gets the client IP"""
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip_number = x_forwarded_for.split(',')[0]
    else:
        ip_number = request.META.get('REMOTE_ADDR')
    return ip_number
