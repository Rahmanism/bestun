"""
WSGI config for bestun project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.10/howto/deployment/wsgi/
"""

import os

#path = '/home/rahmani/w/bestun'
# if path not in sys.path:
#    sys.path.append(path)

#path2 = '/home/rahmani/w/bestun/bestun'
# if path2 not in sys.path:
#    sys.path.append(path2)

#path3 = '/home/rahmani/.local/lib/python3.5/site-packages'
# if path3 not in sys.path:
#    sys.path.append(path3)


# sys.path.append('/home/rahmani')
# sys.path.append('/home/rahmani/w/bestun/web')


#import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "bestun.settings")

application = get_wsgi_application()
