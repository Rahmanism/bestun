# deletes the given files (you can use wildcards) recursively.

param([String]$files="*.orig")
Get-ChildItem -Path .\ -Include $files  -File -Recurse | foreach { $_.Delete()}
