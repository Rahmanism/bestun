angular.module('starter.controllers', [])

  .controller('DashCtrl', function ($scope, $http, $state) {
    $scope.$on('$ionicView.enter', function (e) {
      checkLogin($scope, $state);
      $scope.name = userShowName;
      // angular sends data in json format, but we use the following line
      // to send data as our usual
      $http.defaults.headers.post['Content-Type'] =
        'application/x-www-form-urlencoded;charset=utf-8';

      $http.post(
        baseUrl + '/q/generalstats/',
        'token=' + token
      )
        .success(function (data) {
          $scope.generalstats = data;
        })
        .error(function () {
          $scope.message = 'An error happend on requesting stats.';
          toast($scope.message);
        });
    });
  })

  .controller('ExpensesCtrl', function ($scope, $http, $state) {
    $scope.$on('$ionicView.enter', function (e) {
      checkLogin($scope, $state);

      $http.defaults.headers.post['Content-Type'] =
        'application/x-www-form-urlencoded;charset=utf-8';

      // get expenses list from server
      $http.post(
        baseUrl + '/q/expenses/',
        'token=' + token
      )
        .success(function (data) {
          $scope.expenses = JSON.parse(data);
          $scope.remove = function (expense) {
            Expenses.remove(expense);
          };
        })
        .error(function () {
          $scope.message = 'An error happend on requesting expenses.';
          toast($scope.message);
        });
    });
  })

  .controller('ExpenseDetailCtrl', function ($scope, $stateParams, $http, $state, $stateParams) {
    $scope.$on('$ionicView.enter', function (e) {
      checkLogin($scope, $state);

      $http.defaults.headers.post['Content-Type'] =
        'application/x-www-form-urlencoded;charset=utf-8';

      var sendingData = 'token=' + token +
        '&id=' + $stateParams.expenseId;
      // get expenses list from server
      $http.post(
        baseUrl + '/q/expense/',
        sendingData
      )
        .success(function (data) {
          $scope.expense = JSON.parse(data)[0];
        })
        .error(function () {
          $scope.message = 'An error happend on requesting expense.';
          toast($scope.message);
        });
    });

    // $scope.expense = Expenses.get($stateParams.expenseId);
  })

  .controller('NewExpenseCtrl', function ($scope, $stateParams, $http, $state) {
    $scope.$on('$ionicView.enter', function (e) {
      checkLogin($scope, $state);
    });

    $scope.submit = function () {
      // angular sends data in json format, but we use the following line
      // to send data as our usual
      $http.defaults.headers.post['Content-Type'] =
        'application/x-www-form-urlencoded;charset=utf-8';

      var sendingData = 'token=' + token +
        '&amount=' + $scope.amount +
        '&text=' + $scope.text;
      $http.post(
        baseUrl + '/submit/expense/',
        sendingData
      )
        .success(function (data) {
          $scope.status = data;
          $scope.text = '';
          $scope.amount = '';
          toast('هزینه ثبت شد.');
          $state.go('tab.expenses', null, { reload: true });
        })
        .error(function () {
          $scope.message = 'An error happend on saving expense data.';
          toast($scope.message);
        });
    };
  })

  .controller('IncomesCtrl', function ($scope, $http, $state) {
    $scope.$on('$ionicView.enter', function (e) {
      checkLogin($scope, $state);

      $http.defaults.headers.post['Content-Type'] =
        'application/x-www-form-urlencoded;charset=utf-8';

      // get incomes list from server
      $http.post(
        baseUrl + '/q/incomes/',
        'token=' + token
      )
        .success(function (data) {
          $scope.incomes = JSON.parse(data);
          $scope.remove = function (income) {
            Incomes.remove(income);
          };
        })
        .error(function () {
          $scope.message = 'An error happend on requesting incomes.';
          toast($scope.message);
        });
    });
  })

  .controller('IncomeDetailCtrl', function ($scope, $stateParams, $http, $state, $stateParams) {
    $scope.$on('$ionicView.enter', function (e) {
      checkLogin($scope, $state);

      $http.defaults.headers.post['Content-Type'] =
        'application/x-www-form-urlencoded;charset=utf-8';

      var sendingData = 'token=' + token +
        '&id=' + $stateParams.incomeId;
      // get incomes list from server
      $http.post(
        baseUrl + '/q/income/',
        sendingData
      )
        .success(function (data) {
          $scope.income = JSON.parse(data)[0];
        })
        .error(function () {
          $scope.message = 'An error happend on requesting income.';
          toast($scope.message);
        });
    });

    // $scope.income = incomes.get($stateParams.incomeId);
  })

  .controller('NewIncomeCtrl', function ($scope, $stateParams, $http, $state) {
    $scope.$on('$ionicView.enter', function (e) {
      checkLogin($scope, $state);
    });

    $scope.submit = function () {
      // angular sends data in json format, but we use the following line
      // to send data as our usual
      $http.defaults.headers.post['Content-Type'] =
        'application/x-www-form-urlencoded;charset=utf-8';

      var sendingData = 'token=' + token +
        '&amount=' + $scope.amount +
        '&text=' + $scope.text;

      $http.post(
        baseUrl + '/submit/income/',
        sendingData
      )
        .success(function (data) {
          $scope.status = data;
          $scope.text = '';
          $scope.amount = '';
          toast('درآمد ثبت شد.');
          $state.go('tab.incomes', null, { reload: true });
        })
        .error(function () {
          $scope.message = 'An error happend on saving income data.';
          toast($scope.message);
        });
    };
  })

  /// Settings Controller
  .controller('SettingsCtrl', function ($scope, $http, $state, $ionicHistory) {
    // check if the user is logged in and set the flag.
    $scope.loggedin = false;

    token = storage.getItem('token');
    if (token) {
      $scope.loggedin = true;
      name
    }

    /// Login form submit
    $scope.submit = function () {
      $scope.message = '';
      $http.defaults.headers.post['Content-Type'] =
        'application/x-www-form-urlencoded;charset=utf-8';

      $http.post(
        baseUrl + '/accounts/login/',
        'username=' + $scope.username + '&password=' + $scope.password
      )
        .success(function (data) {
          // if login was successful
          if (data.result == 'ok') {
            token = data.token;
            storage.setItem('token', token);
            userShowName = $scope.username;
            storage.setItem('userShowName', userShowName);
            $scope.loggedin = true;
            stateReload();
            toast('با موفقیت وارد سیستم شدید.');
          }
          else {
            token = null;
            $scope.loggedin = false;
            $scope.message = 'login faild!';
            toast($scope.message);
          }
        })
        .error(function () {
          $scope.message = 'An error happend on requesting login.';
          toast($scope.message);
          token = null;
          $scope.loggedin = false;
        });
    };

    $scope.logout = function () {
      token = null;
      storage.removeItem('token');
      userShowName = 'مهمان';
      storage.removeItem('userShowName');
      $scope.loggedin = false;
      stateReload();
    }

    stateReload = function () {
      $ionicHistory.clearCache([$state.current.name]).then(function () {
        $state.reload();
      });
    }
  })

  .controller('LinksCtrl', function ($scope, $http) {
    $http.get('http://hozehkh.com/links-json.aspx?location=2')
      .success(function (data) {
        if (data.constructor === Array && data.length > 0) {
          $scope.links = data;
          $scope.links.forEach(function (item, index) {
            item.id = item.fserial;
            item.img = 'http://hozehkh.com/EntriesImages/links/' + item.fserial + '.jpg';
            if (!item.url.startsWith('http')) {
              item.url = 'http://hozehkh.com' + item.url;
            }
          });
        }
        else {
          $scope.message = 'There is no link.';
          toast($scope.message);
        }
      })
      .error(function () {
        $scope.message = 'Error in loading links.';
        toast($scope.message);
      });
  })

  .controller('LinkDetailCtrl', function ($scope, $stateParams, id, $state) {
    // TODO: get the link id and send it to the html template

    // for (var i = 0; i < $scope.links.length; i++){
    // }
    // $scope.link = $scope.links.;
  })


  /// Gets the notes from server
  .controller('NotesCtrl', function ($scope, $http, $state) {
    // get expenses list from server
    $http.get(baseUrl + '/q/notes/10')
      .success(function (data) {
        $scope.notes = JSON.parse(data);
      })
      .error(function () {
        $scope.message = 'An error happend on requesting notes.';
        toast($scope.message);
      });
  })


  ;