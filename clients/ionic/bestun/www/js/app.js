// some (almost) global variables
// var baseUrl = 'http://127.0.0.1:8000';
var baseUrl = 'http://217.219.79.180';
// var baseUrl = 'http://192.168.43.247:8000';
var loggedin = false;
var userMessage = '';
var storage = window.localStorage;
var token = storage.getItem('token');
var userShowName = storage.getItem('userShowName');
if (!userShowName) {
    userShowName = 'مهمان';
}

/// redirect to login page
function backToLoginPage($scope, $state) {
    token = null;
    loggedin = false;
    storage.removeItem('token');
    storage.removeItem('userShowName');
    $state.go('tab.settings');
}

/// checks if user not logged in, redirects to login page
function checkLogin($scope, $state) {
    if (token == null) {
        backToLoginPage($scope, $state);
    }
}

// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'ngSanitize', 'ionic-toast'])

    .run(function ($ionicPlatform, ionicToast) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }

            toast = function (msg) {
                // ionicToast.show(message, position, stick, time);
                ionicToast.show(msg, 'bottom', true, 2500);
            };
        });
    })

    .config(function ($stateProvider, $urlRouterProvider) {

        // Ionic uses AngularUI Router which uses the concept of states
        // Learn more here: https://github.com/angular-ui/ui-router
        // Set up the various states which the app can be in.
        // Each state's controller can be found in controllers.js
        $stateProvider

            // setup an abstract state for the tabs directive
            .state('tab', {
                url: '/tab',
                abstract: true,
                templateUrl: 'templates/tabs.html'
            })

            // Each tab has its own nav history stack:

            .state('tab.dash', {
                url: '/dash',
                views: {
                    'tab-dash': {
                        templateUrl: 'templates/tab-dash.html',
                        controller: 'DashCtrl'
                    }
                }
            })

            .state('tab.settings', {
                url: '/settings',
                views: {
                    'tab-settings': {
                        templateUrl: 'templates/tab-settings.html',
                        controller: 'SettingsCtrl'
                    }
                }
            })

            .state('tab.links', {
                url: '/links',
                views: {
                    'tab-links': {
                        templateUrl: 'templates/tab-links.html',
                        controller: 'LinksCtrl'
                    }
                }
            })
            .state('tab.link-detail', {
                url: '/links/:id',
                views: {
                    'tab-links': {
                        templateUrl: 'templates/link-detail.html',
                        controller: 'LinkDetailCtrl'
                    }
                }
            })


            .state('tab.expenses', {
                url: '/expenses',
                views: {
                    'tab-expenses': {
                        templateUrl: 'templates/tab-expenses.html',
                        controller: 'ExpensesCtrl'
                    }
                }
            })
            // .state('tab.new-expense', {
            //     url: '/expenses/new',
            //     views: {
            //         'tab-expenses': {
            //             templateUrl: 'templates/new-expense.html',
            //             controller: 'NewExpenseCtrl'
            //         }
            //     }
            // })
            .state('tab.expense-detail', {
                url: '/expenses/:expenseId',
                views: {
                    'tab-expenses': {
                        templateUrl: 'templates/expense-detail.html',
                        controller: 'ExpenseDetailCtrl'
                    }
                }
            })

            .state('tab.incomes', {
                url: '/incomes',
                views: {
                    'tab-incomes': {
                        templateUrl: 'templates/tab-incomes.html',
                        controller: 'IncomesCtrl'
                    }
                }
            })
            // .state('tab.new-income', {
            //     url: '/incomes/new',
            //     views: {
            //         'tab-incomes': {
            //             templateUrl: 'templates/new-income.html',
            //             controller: 'NewIncomeCtrl'
            //         }
            //     }
            // })
            .state('tab.income-detail', {
                url: '/incomes/:incomeId',
                views: {
                    'tab-incomes': {
                        templateUrl: 'templates/income-detail.html',
                        controller: 'IncomeDetailCtrl'
                    }
                }
            })

            ;

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/tab/dash');

    });