angular.module('starter.services', [])

  .factory('Expenses', function () {
    // Might use a resource here that returns a JSON array

    // Some fake testing data
    var expenses = [{
      id: 0,
      text: 'Ben Sparrow',
      amount: '15',
      face: 'img/ben.png'
    }, {
      id: 1,
      text: 'Max Lynx',
      amount: '12.5',
      face: 'img/max.png'
    }, {
      id: 2,
      text: 'Adam Bradleyson',
      amount: '3.4',
      face: 'img/adam.jpg'
    }, {
      id: 3,
      text: 'Perry Governor',
      amount: '9.3',
      face: 'img/perry.png'
    }, {
      id: 4,
      text: 'Mike Harrington',
      amount: '11',
      face: 'img/mike.png'
    }];

    return {
      all: function () {
        return expenses;
      },
      remove: function (expense) {
        expenses.splice(expenses.indexOf(expense), 1);
      },
      get: function (expenseId) {
        for (var i = 0; i < expenses.length; i++) {
          if (expenses[i].id === parseInt(expenseId)) {
            return expenses[i];
          }
        }
        return null;
      }
    };
  })
  ;
